# installation

clone the repo

```bash
https://gitlab.com/reedrichards/quick-search
```

install the dependencies

```bash
npm install
```

run the program

```
npm run
```
