import React from "react";
const { clipboard } = require("electron");
const execSync = require("child_process").execSync;
import read from "read-yaml";
import * as os from "os";
var config = read.sync(os.homedir() + "/.config/quick-search/config.yaml");

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clipboard: clipboard.readText("selection"),
      value: "",
      suggestions: []
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.nameInput.focus();
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
    let query = this.buildQuery(event.target.value);
    const request = async () => {
      const response = await fetch(
        "http://suggestqueries.google.com/complete/search?client=firefox&q=" +
          query
      );
      const json = await response.json();
      this.setState({ suggestions: json[1] });
    };
    request();
  }

  buildQuery(val) {
    if (val === "") {
      return this.state.clipboard.replace(/ /gi, "+").replace(/"/gi, '\\"');
    }
    return val.replace(/ /gi, "+").replace(/"/gi, '\\"');
  }

  handleSubmit(event) {
    event.preventDefault();
    const query = this.buildQuery();
    execSync('xdg-open "' + config[config.default].url + query + '"');
    window.close();
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <input
            ref={input => {
              this.nameInput = input;
            }}
            placeholder={this.state.clipboard}
            value={this.state.value}
            onChange={this.handleChange}
          />

          <input type="submit" value="Submit" />

          {this.state.suggestions.map(s => {
            return <p key={s}>{s}</p>;
          })}
        </form>
      </div>
    );
  }
}
