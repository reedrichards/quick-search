# Quick Search

 [Gitlab](https://gitlab.com/reedrichards/quick-search)

 [Docs](http://localhost:8000/#/docs/quick-search/README)

## Overview

a utility to quickly search google, wikipedia and more. Desigined to be tied to a keyboard macro to quickly search the web in your default browser, without having to context switch.

## Install

TODO

## Usage

### launch

launch the application with
```bash
quick-search
```

The default search engine is google, but can be over ridden by offering a command line arguement.

```bash
quick-search wikipedia
quick-search stackoverflow
```


### search

#### default behavior

<!--
TODO add link
-->
The default search engine is the search engine defined as `default` in `~/.config/quick-search/config.yaml`. The default parameter for this is `Google`. You can learn more about editing this as [default](#).

<!--
TODO add a gif of flow here
-->
The default search parameter is your clipboard, and is displayed as the default value of the search bar. Hitting enter will automatically search your clipboard. Entering any value will override the default value.


#### override default search engine
no matter what you use as the default search engine, it can be overridden in the search bar with a `/` arguement

* Searches wikipedia for: Godels Incompletness therom

`/w Godels Incompletness therom`

* Searches stackoverflow for: how to exit vim?

` /s how to exit vim?`

## Configuration


### Config Location

configuration files are located in the following location.

```
~/.config/quick-search/config.yaml
```

### Sample Config Location

configuration files can be either yaml or json and an example config is available at

```
~/.config/quick-search/example-config.yaml
```
### Sample Config

```yaml
default: "google"

google:
  url: "https://www.google.com/search?q="
  cli-arg: "google"
  prefix-arg: "g"
stackoverflow:
  url: "TODO"
  cli-arg: "stackoverflow"
  prefix-arg: "s"
wikipedia:
  url: "TODO"
  cli-arg: "wikipedia"
  prefix-arg: "w"
```

### Parameters

### `url`
* **type**: `string`

`quick-search` builds a search by appending the `input` to the end of a `url` configuration of a search engine. `urls`'s defined in the config are actively supported and maintained but additional `urls` are not guaranteed to work.


### `cli-arg`
* **type**: `string`

defines the command line argument to launch the search engine as the default search engine.

### `prefix-arg`
* **type**: `string`

defines the prefix to override default search engine in gui.

#### example
##### config
```yaml
search-engines:
  google:
    - url: "https://www.google.com/search?q="
      cli-arg: "google"
      prefix-arg: "g"
```
##### usage
```
/g answer to life, the universe, and everything
```

### `default`
* **type**: `string`

the default search engine to be used when `quick-search` is run with no arguemnts




## Advanced
 TODO

### Intergration with `gnome`
### Intergration with `KDE`
### Intergration with `i3wm`
### Intergration with `Windows 10`
### Intergration with `MacOS`
